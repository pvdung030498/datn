import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './index.css'
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'
import 'boxicons/css/boxicons.css'

const app = createApp(App)
import AOS from 'aos'
import 'aos/dist/aos.css'
app.AOS = new AOS.init({ disable: 'phone' })
app.use(router).use(store).mount('#app')
