import axios from 'axios';
export default {
  state(){
     return {
       product: null,
     }
  },

  getters: {
    product: (state) => {
      console.log(state.product);
      return state.product
    }
  },

  mutations: {
    setProduct(state, payload){
       this.state.product = payload
      //  console.log(state.product);
    }
  },

  actions: {
    fetchProduct({ commit }, {id}) {
      axios.get("http://localhost:3001/promotions/" + id)
      .then(response => {
        const a = response.data;
        commit('setProduct', a)
      })
      .catch(e => {
        this.errors.push(e)
      })
    },
    // async fetchProduct({ commit }, { id }){
    //   const res = await fetch("http://localhost:3001/promotions/" + id)
    //   const data = await res.json()
    //   commit("setProduct", data)
    // }
  },
}
