import { createRouter, createWebHistory } from 'vue-router'
import Home from '/src/views/Home.vue'
import Iphone from '/src/views/Iphone.vue'
import Dienthoai from '/src/views/Dienthoai.vue'
import Tablet from '/src/views/Tablet.vue'
import Macbook from '/src/views/Macbook.vue'
import Hangcu from '/src/views/Hangcu.vue'
import Phukien from '/src/views/Phukien.vue'
import Dongho from '/src/views/Dongho.vue'
import Giohang from '/src/views/Giohang.vue'
import Dangky from '/src/views/Dangky.vue'
import Dangnhap from '/src/views/Dangnhap.vue'
import User from '/src/views/User.vue'
import PageNotFound from '/src/views/PageNotFound.vue'

import DanhMuc from '/src/views/DanhMuc.vue'
import Product from '/src/views/Product.vue'

const routes = [
    {
        path: '/',
        name: 'home',
        component: Home,
    },
    {
        path: '/danhmuc',
        component: DanhMuc,
        children: [
            {
                path: 'iphone-moi-chinh-hang',
                name: 'iphone',
                component: Iphone,
            },
            {
                path: 'dienthoai',
                name: 'dienThoai',
                component: Dienthoai,
            },
            {
                path: 'tablet',
                name: 'tablet',
                component: Tablet,
            },
            {
                path: 'macbook',
                name: 'macbook',
                component: Macbook,
            },
            {
                path: 'hangcu',
                name: 'hangcu',
                component: Hangcu,
            },
            {
                path: 'phukien',
                name: 'phukien',
                component: Phukien,
            },
            {
                path: 'dongho',
                name: 'dongho',
                component: Dongho,
            },
        ],
    },
    {
        path: '/giohang',
        name: 'giohang',
        component: Giohang,
    },

    {
        path: '/dangky',
        name: 'dangky',
        component: Dangky,
    },
    {
        path: '/dangnhap',
        name: 'dangnhap',
        component: Dangnhap,
    },

    {
        path: '/user',
        name: 'user',
        component: User,
    },

    {
        path: '/:id',
        name: 'product',
        component: Product,
        beforeEnter: (to, from, next) => {
            let temp = to.params.id.split('-')[1]

            var number = parseInt(temp)

            const getProducts = async () => {
                try {
                    const results = await fetch(
                        'http://localhost:3001/promotions'
                    )
                    const products = await results.json()
                    return products
                } catch (err) {
                    console.log(err)
                }
            }
            async function main() {
                var a = await getProducts()
                const c = a.map((b) => {
                    return b.id
                })
                const d = c.includes(number)
                // console.error(c);
                if (d) {
                    next()
                } else {
                    next({ name: 'error' })
                }
            }
            main()
        },
    },
    {
        path: '/:pathMatch(.*)*',
        name: 'error',
        component: PageNotFound,
    },
]

const router = createRouter({
    history: createWebHistory(),
    routes,
    scrollBehavior() {
        window.scrollTo(0, 0)
    },
})

export default router
